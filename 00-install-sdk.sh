#!/bin/bash
# A script to install gcloud and dependancies

# go home
cd ~

# install epel
yum -y install epel-release

# install git
yum -y install git

# add repo
sudo tee -a /etc/yum.repos.d/google-cloud-sdk.repo << EOM
[google-cloud-sdk]
name=Google Cloud SDK
baseurl=https://packages.cloud.google.com/yum/repos/cloud-sdk-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
       https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOM

# get the SDK and wget 
yum install -y google-cloud-sdk
yum install -y wget google-cloud-sdk-app-engine-java

# install kubectl
yum install -y kubectl

# grab sock shop yaml
wget https://raw.githubusercontent.com/microservices-demo/microservices-demo/master/deploy/kubernetes/complete-demo.yaml

# initialize gcloud
gcloud init 

